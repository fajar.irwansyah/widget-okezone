<?php

include "config.php";

// has moved to config 
// $test_current_page = "update-widget-test.php";
// $test_urlconfigjs = "shareconf/config-widget-test.js";
// $test_urlconfigphp = "config-widget-test.php";
// $test_urlwidget = "widget-dark-test.html";


if (!is_dir ($dirshareconf)){
    mkdir($dirshareconf, 0777, true);
}

$filename = $test_urlconfigphp;
if (file_exists($filename)){
    include $test_urlconfigphp;
}


function getToken($urlBase,$device_id,$platform,$username,$password){
    $data = array(
        'device_id' => $device_id
        ,'platform' => $platform
        ,'username' => $username
        ,'password' => $password
    );
    
    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/json\r\n",
            'method'  => 'POST',
            'content' => json_encode($data),
        ),
    );
    $context  = stream_context_create($options);
    $url = $urlBase . '/api/v/login';
    $result = file_get_contents($url, false, $context);    
    // var_dump($result);
    $result = utf8_encode($result);
    $data = json_decode($result,true);
    
    if ($data['status']['code'] == 18)    
        return $data['data']['access_token']; //token
    else
        echo 'error token';
}

function getImageWide($urlBase,$token,$contentID){
    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'method'  => 'GET',
            'header'  => "Content-type: application/json\r\n".
                "Authorization: ".$token."\r\n"
        ),
    );

    $context  = stream_context_create($options);
    $url=$urlBase . "/api/v5/contents/" . $contentID;
    $result = file_get_contents($url, false, $context);    
    // var_dump($result);
    $result = utf8_encode($result);
    $data = json_decode($result,true);
    
    if ($data['status']['code'] == 11)    
        return $data['data']['image_url'];
    else
        echo "error get image id:".$contentID.'<br>';
}

function getImagePortrait($urlBase,$token,$contentID){
    // use key 'http' even if you send the request to https://...
    $options = array(
            'http' => array(
            'method'  => 'GET',
            'header'  => "Content-type: application/json\r\n".
                "Authorization: ".$token."\r\n"
        ),
    );

    $context  = stream_context_create($options);
    $url=$urlBase . "/api/v5/contents/" . $contentID;
    $result = file_get_contents($url, false, $context);    
    // var_dump($result);
    $result = utf8_encode($result);
    $data = json_decode($result,true);
    
    if ($data['status']['code'] == 11)    
        return $data['data']['thumb_url'];
    else
        echo "error get image id:".$contentID.'<br>';
}

function createLink($urlBase,$token,$contentID){
    // use key 'http' even if you send the request to https://...
    $options = array(
            'http' => array(
            'method'  => 'GET',
            'header'  => "Content-type: application/json\r\n".
                "Authorization: ".$token."\r\n"
        ),
    );

    $context  = stream_context_create($options);
    $url=$urlBase . "/api/v5/contents/" . $contentID;
    $result = file_get_contents($url, false, $context);    
    // var_dump($result);
    $result = utf8_encode($result);
    $data = json_decode($result,true);    

    if ($data['status']['code'] == 11)    
        return 'https://visionplus.okezone.com/play/' . $data['data']['id'] . '-' . str_replace(' ', '-', trim($data['data']['title']));
    else
        echo "error create link id:".$contentID.'<br>';
}

if (empty($_COOKIE['password']) || $_COOKIE['password'] !== md5($configpassword)) {
    // Password not set or incorrect. Send to login.php.
    echo "
        <table align=center>
        <tr valign=top><td>Password required to update widget</td></tr>
        <tr valign=top>
            <td>
                <form action=$test_current_page method='post'>
                    Password <input type='password' name='password'><br>
                    <input type='submit' value='Submit'>
                </form>
            </td>
        </tr>
        </table>
    ";

    /* Redirects here after login */
    $redirect_after_login = $test_current_page;

    /* Will not ask password again for */
    $remember_password = strtotime('+1 days'); // 1 days

    if (isset($_POST['password'])) {
        $password = $_POST['password'];
            setcookie("password", md5($password), $remember_password);
            header('Location: ' . $redirect_after_login);
            exit;
    }
    exit;
}else{

    //get token from API
    $token = getToken($urlBase,$device_id,$platform,$username,$password);

    if (isset($_POST["content01"])) {
        $content01 = $_POST["content01"];
        $moviebanner=getImageWide($urlBase,$token,$content01);
        $linkbanner=createLink($urlBase,$token,$content01);
    }
    if (isset($_POST["content02"])) {
        $content02 = $_POST["content02"];
        $movie01=getImagePortrait($urlBase,$token,$content02) ;
        $link01=createLink($urlBase,$token,$content02) ;
    }
    if (isset($_POST["content03"])) {
        $content03 = $_POST["content03"];
        $movie02=getImagePortrait($urlBase,$token,$content03) ;
        $link02=createLink($urlBase,$token,$content03) ;
    }
    if (isset($_POST["content04"])) {
        $content04 = $_POST["content04"];
        $movie03=getImagePortrait($urlBase,$token,$content04) ;
        $link03=createLink($urlBase,$token,$content04) ;
    }
    if (isset($_POST["content05"])) {
        $content05 = $_POST["content05"];
        $movie04=getImagePortrait($urlBase,$token,$content05) ;
        $link04=createLink($urlBase,$token,$content05) ;
    }
    if (isset($_POST["content06"])) {
        $content06 = $_POST["content06"];
        $movie05=getImagePortrait($urlBase,$token,$content06) ;
        $link05=createLink($urlBase,$token,$content06) ;
    }
    if (isset($_POST["content07"])) {
        $content07 = $_POST["content07"];
        $movie06=getImagePortrait($urlBase,$token,$content07) ;
        $link06=createLink($urlBase,$token,$content07) ;
    }
    if (isset($_POST["content08"])) {
        $content08 = $_POST["content08"];
        $movie07=getImagePortrait($urlBase,$token,$content08) ;
        $link07=createLink($urlBase,$token,$content08) ;
    }
    if (isset($_POST["content09"])) {
        $content09 = $_POST["content09"];
        $movie08=getImagePortrait($urlBase,$token,$content09) ;
        $link08=createLink($urlBase,$token,$content09) ;
    }
    if (isset($_POST["content10"])) {
        $content10 = $_POST["content10"];
        $movie09=getImagePortrait($urlBase,$token,$content10) ;
        $link09=createLink($urlBase,$token,$content10) ;
    }

    if (!isset($content01)) $content01 = "28720";

    if (!isset($content02)) $content02 = "28613";
    if (!isset($content03)) $content03 = "22926";
    if (!isset($content04)) $content04 = "345";

    if (!isset($content05)) $content05 = "18569";
    if (!isset($content06)) $content06 = "17465";
    if (!isset($content07)) $content07 = "18397";

    if (!isset($content08)) $content08 = "24661";
    if (!isset($content09)) $content09 = "1639";
    if (!isset($content10)) $content10 = "28693";

    if (!isset($linkbanner)) $linkbanner = createLink($urlBase,$token,$content01);
    if (!isset($moviebanner)) $moviebanner = getImageWide($urlBase,$token,$content01);
    if (!isset($link01)) $link01 = createLink($urlBase,$token,$content02);
    if (!isset($movie01)) $movie01 = getImagePortrait($urlBase,$token,$content02);
    if (!isset($link02)) $link02 = createLink($urlBase,$token,$content03);
    if (!isset($movie02)) $movie02 = getImagePortrait($urlBase,$token,$content03);
    if (!isset($link03)) $link03 = createLink($urlBase,$token,$content04);
    if (!isset($movie03)) $movie03 = getImagePortrait($urlBase,$token,$content04);
    if (!isset($link04)) $link04 = createLink($urlBase,$token,$content05);
    if (!isset($movie04)) $movie04 = getImagePortrait($urlBase,$token,$content05);
    if (!isset($link05)) $link05 = createLink($urlBase,$token,$content06);
    if (!isset($movie05)) $movie05 = getImagePortrait($urlBase,$token,$content06);
    if (!isset($link06)) $link06 = createLink($urlBase,$token,$content07);
    if (!isset($movie06)) $movie06 = getImagePortrait($urlBase,$token,$content07);
    if (!isset($link07)) $link07 = createLink($urlBase,$token,$content08);
    if (!isset($movie07)) $movie07 = getImagePortrait($urlBase,$token,$content08);
    if (!isset($link08)) $link08 = createLink($urlBase,$token,$content09);
    if (!isset($movie08)) $movie08 = getImagePortrait($urlBase,$token,$content09);
    if (!isset($link09)) $link09 = createLink($urlBase,$token,$content10);
    if (!isset($movie09)) $movie09 = getImagePortrait($urlBase,$token,$content10);



    echo "
        <table height=700>
        <tr valign=top>
            <td width=400>
                <iframe src=$test_urlwidget title='Widget Test' height=650></iframe>
            </td>
            <td>
                <form action=$test_current_page method='post'>
                <input type='hidden' name='update' value='1'><br>
                Banner 01: <input type='text' name='content01' value='$content01'><br>

                Banner 02: <input type='text' name='content02' value='$content02'><br>
                Banner 03: <input type='text' name='content03' value='$content03'><br>
                Banner 04: <input type='text' name='content04' value='$content04'><br>

                Banner 05: <input type='text' name='content05' value='$content05'><br>
                Banner 06: <input type='text' name='content06' value='$content06'><br>
                Banner 07: <input type='text' name='content07' value='$content07'><br>

                Banner 08: <input type='text' name='content08' value='$content08'><br>
                Banner 09: <input type='text' name='content09' value='$content09'><br>
                Banner 10: <input type='text' name='content10' value='$content10'><br>
                <input type='submit' value='Update Widget'>
                </form>
            </td>
        </tr>
        </table>
    ";

    if (isset($_POST["update"]) && ($_POST["update"] == 1)) {

        $content ='
        <?php
        $content01 = "'.$content01.'";
        $content02 = "'.$content02.'";
        $content03 = "'.$content03.'";
        $content04 = "'.$content04.'";
        $content05 = "'.$content05.'";
        $content06 = "'.$content06.'";
        $content07 = "'.$content07.'";
        $content08 = "'.$content08.'";
        $content09 = "'.$content09.'";
        $content10 = "'.$content10.'";

        $moviebanner = "'.$moviebanner.'";
        $linkbanner = "'.$linkbanner.'";
        $link01 = "'.$link01.'";
        $movie01 = "'.$movie01.'";
        $link02 = "'.$link02.'";
        $movie02 = "'.$movie02.'";
        $link03 = "'.$link03.'";
        $movie03 = "'.$movie03.'";
        $link04 = "'.$link04.'";
        $movie04 = "'.$movie04.'";
        $link05 = "'.$link05.'";
        $movie05 = "'.$movie05.'";
        $link06 = "'.$link06.'";
        $movie06 = "'.$movie06.'";
        $link07 = "'.$link07.'";
        $movie07 = "'.$movie07.'";
        $link08 = "'.$link08.'";
        $movie08 = "'.$movie08.'";
        $link09 = "'.$link09.'";
        $movie09 = "'.$movie09.'";

        ?>
        ';

        //write config in php
        file_put_contents($test_urlconfigphp, $content);


        $content = '
        var wconfig = {
            content01 : "'.$content01.'",
            content02 : "'.$content02.'",
            content03 : "'.$content03.'",
            content04 : "'.$content04.'",

            content05 : "'.$content05.'",
            content06 : "'.$content06.'",
            content07 : "'.$content07.'",

            content08 : "'.$content08.'",
            content09 : "'.$content09.'",
            content10 : "'.$content10.'",

            moviebanner : "'.$moviebanner.'",
            linkbanner : "'.$linkbanner.'",
            link01 : "'.$link01.'",
            movie01 : "'.$movie01.'",
            link02 : "'.$link02.'",
            movie02 : "'.$movie02.'",
            link03 : "'.$link03.'",
            movie03 : "'.$movie03.'",
            link04 : "'.$link04.'",
            movie04 : "'.$movie04.'",
            link05 : "'.$link05.'",
            movie05 : "'.$movie05.'",
            link06 : "'.$link06.'",
            movie06 : "'.$movie06.'",
            link07 : "'.$link07.'",
            movie07 : "'.$movie07.'",
            link08 : "'.$link08.'",
            movie08 : "'.$movie08.'",
            link09 : "'.$link09.'",
            movie09 : "'.$movie09.'"
        };
        ';
        //write config in js config
        file_put_contents($test_urlconfigjs, $content);
    }
}

?>
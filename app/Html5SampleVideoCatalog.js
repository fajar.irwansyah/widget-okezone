/*! (C) 2016 Conviva, Inc. All rights reserved. Confidential and proprietary. */
/*! This is sample code meant to illustrate proper Conviva integration in video applications. */
/*! This file should not be included in video applications as part of integrating Conviva. */

/// Listing of available videos

var VideoCatalog = function () {
  this._videos = [
    {
      url: "http://content.bitsontherun.com/videos/QieeAOba-jt16QglC.mp4",
      previewUrl: "http://d3el35u4qe4frz.cloudfront.net/QieeAOba-480.jpg",
      metadata: {
        id: "video1",
        title: "wacky streams",
        live: false,
        bitrateKbps: 100,
        durationSec: 89,
        frameRate: 24,
        genres: ["Music", "Nature"],
      }
    },
    {
      url: "http://content.bitsontherun.com/videos/njZMLMwy-jt16QglC.mp4",
      previewUrl: "http://d3el35u4qe4frz.cloudfront.net/njZMLMwy-480.jpg",
      metadata: {
        id: "video2",
        title: "driving rain",
        live: false,
        bitrateKbps: 200,
        durationSec: 241,
        frameRate: 24,
        genres: ["Music", "Nature"],
      }
    },
    {
      url: "http://content.bitsontherun.com/videos/Vg7cPUdl-jt16QglC.mp4",
      previewUrl: "http://d3el35u4qe4frz.cloudfront.net/Vg7cPUdl-480.jpg",
      metadata: {
        id: "video3",
        title: "winter brook",
        live: false,
        bitrateKbps: 300,
        durationSec: 91,
        frameRate: 24,
        genres: ["Music", "Nature"],
      }
    }
  ];

  this.getDataForVideo = function (videoIndex) {
    return this._videos[videoIndex];
  };
};

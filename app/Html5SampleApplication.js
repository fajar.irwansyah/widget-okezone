/*! (C) 2016 Conviva, Inc. All rights reserved. Confidential and proprietary. */
/*! This is sample code meant to illustrate proper Conviva integration in video applications. */
/*! This file should not be included in video applications as part of integrating Conviva. */

/// Sample application in charge of UI display and content selection.

var SampleApp = function (mode, user, videoCatalog, logger, eventBus) {

  // "this" binding for callbacks
  var self = this;

  // Active video player
  var videoPlayer = null;

  var previews = [];

  ///
  /// Constructor
  ///

  function _constr(config, user, videoCatalog, logger, eventBus) {
    this._config = config;
    this._user = user;
    this._videoCatalog = videoCatalog;
    this._logger = logger;
    this._eventBus = eventBus;

    this._environment = this._config["environment"];
    this._enableConviva = this._config["enableConviva"];
  }

  _constr.apply(this, arguments);

  ///
  /// High-level application flow
  ///

  this.start = function (applicationMode, user, videoCatalog, logger, eventBus) {
    this._logger.log("Start");

    this._eventBus.publish(EventBus.Event.APPLICATION_START, this._config);

    this.displayVideoPreviews();
  };

  this.stop = function () {
    if (videoPlayer != null) { // a video was already playing
      self.onPlaybackCancel();
    }

    this.removeVideoPreviews();

    this._logger.log("Cleanup");
    this._eventBus.publish(EventBus.Event.APPLICATION_STOP);

  };

  // Called when user requests playback for one of the videos.
  this.onPlaybackRequested = function (catalogId) {
    this._logger.log("Playback Requested");

    // Get metadata for the requested video
    var videoData = this.fetchVideoData(catalogId);

    this._eventBus.publish(EventBus.Event.VIDEO_PLAYBACK_REQUEST, this._user, videoData);
    var streamUrl = videoData.url;
    this.playSelectedVideo(streamUrl);
  };

  // Called when user cancels current playback
  this.onPlaybackCancel = function () {
    // remove the current video player
    this.removeVideoPlayer();

    this._logger.log("Playback Cancelled");
    this._eventBus.publish(EventBus.Event.VIDEO_PLAYBACK_CANCELLED);
  };

  // Called when current video ends
  this.onPlaybackEnded = function () {
    // remove the current video player
    this.removeVideoPlayer();

    this._logger.log("Playback Ended");
    this._eventBus.publish(EventBus.Event.VIDEO_PLAYBACK_ENDED);
  };

  // Called when current video playback fails.
  this.onPlaybackFailed = function () {
    // remove the current video player
    this.removeVideoPlayer();

    this._logger.log("Playback Failed");
    this._eventBus.publish(EventBus.Event.VIDEO_PLAYBACK_FAILED);
  };

  // Called when user picks a side video
  this.onPreviewsSwapped = function () {
    this._logger.log("Previews Swapped");
  };

  ///
  /// Low-level internals
  ///

  this.removeVideoPlayer = function () {
    if (videoPlayer != null) { // we have a player displayed already
      this._logger.log("Remove Video Player");
      videoPlayer.remove(); // get rid of it
      videoPlayer = null;

      this._eventBus.publish(EventBus.Event.VIDEO_PLAYER_DESTROYED);
    }
    previews[1].show(); // show the preview image again
  };

  this.createVideoPlayer = function (videoStreamUrl) {
    this._logger.log("Create Video Player");

    var endedCallback = function () {
      self.onPlaybackEnded();
    };
    var failureCallback = function () {
      self.onPlaybackFailed();
    };
    var container = $('.center');
    videoPlayer = new VideoPlayer(container, videoStreamUrl, endedCallback, failureCallback);
    videoPlayer.create();

    this._eventBus.publish(EventBus.Event.VIDEO_PLAYER_CREATED, videoPlayer);
  };

  this.playVideo = function () {
    this._logger.log("Play Video");

    previews[1].hide();
    videoPlayer.show();
    // DE-3450: play() does not have effect in safari browsers unless there is a small delay
    // Unhandled promise rejection in Safari is raised if called without setTimeout
    setTimeout(function() {
      videoPlayer.play();
    },1);
  };

  this.fetchVideoData = function (selectedVideoIndex) {
    this._logger.log("Fetch Video Data");

    var videoData = this._videoCatalog.getDataForVideo(selectedVideoIndex);
    return videoData;
  };

  this.playSelectedVideo = function (videoStreamUrl) {
    // Remove the previous video player if any
    this.removeVideoPlayer();

    // Create a new video player to play the video
    this.createVideoPlayer(videoStreamUrl);

    // Start playing
    this.playVideo();
  };

  var clickCallback = function (e) {
    var clickedPreview = $(e.currentTarget);
    var selectedVideoId = clickedPreview.attr('id');
    if (clickedPreview.parent().hasClass('center')) { // center video has been clicked
      self.onPlaybackRequested(selectedVideoId); // play it
    } else { // side video has been clicked
      if (videoPlayer != null) { // if a video was already playing
        self.onPlaybackCancel(); // stop playback
      }
      var clickedPreviewAreaId = clickedPreview.parent().hasClass('left') ? 0 : 2;
      self.swapWithCenter(clickedPreviewAreaId); // move it to the center
    }
  };

  this.testContentMutableMetadata = function (e) {
      this._eventBus.publish(EventBus.Event.CONTENT_MUTABLE_METADATA_REQUEST, null, null);
  };

  this.testContentSetBitrateKbps = function (e) {
      this._eventBus.publish(EventBus.Event.CONTENT_SET_BITRATE_KBPS_REQUEST);
  };

  this.testContentSendCustomEvent = function(e) {
      this._eventBus.publish(EventBus.Event.CONTENT_SEND_CUSTOM_EVENT_REQUEST);
  };

  this.testContentReportError = function(e) {
      this._eventBus.publish(EventBus.Event.CONTENT_REPORT_ERROR_REQUEST);
  };

  this.testContentSetCDNServerIP = function() {
    this._eventBus.publish(EventBus.Event.CONTENT_SET_CDN_SERVER_IP);
  };

  this.testCreateAdSession = function (e) {
      // Can use same videoPlayer object for sumilating ad playback
      this._eventBus.publish(EventBus.Event.CREATE_AD_SESSION_REQUEST, videoPlayer);
  };

  this.testAdMutableMetadata = function (e) {
      this._eventBus.publish(EventBus.Event.AD_MUTABLE_METADATA_REQUEST);
  };

  this.testAdSetBitrateKbps = function (e) {
      this._eventBus.publish(EventBus.Event.AD_SET_BITRATE_KBPS_REQUEST);
  };

  this.testAdSendCustomEvent = function(e) {
      this._eventBus.publish(EventBus.Event.AD_SEND_CUSTOM_EVENT_REQUEST);
  };

  this.testAdReportError = function(e) {
      this._eventBus.publish(EventBus.Event.AD_REPORT_ERROR_REQUEST);
  };

  this.testCloseAdSession = function (e) {
      this._eventBus.publish(EventBus.Event.CLOSE_AD_SESSION_REQUEST);
  };

  this.swapWithCenter = function (swapee) {
    previews[swapee].replaceWith(previews[1]);
    $('.center').append(previews[swapee]);
    previews[swapee].click(clickCallback); // rebind, disappears in the swap.

    var tmp = previews[1];
    previews[1] = previews[swapee];
    previews[swapee] = tmp;

    previews[swapee].attr({
      "data-content": "Select"
    });

    previews[1].attr({
      "data-content": "Play"
    });

    self.onPreviewsSwapped();
  };

  this.displayVideoPreviews = function () {
    this._logger.log("Display Video Previews");

    previews[0] = $('<div></div>')
      .attr({
        id: "0",
        "data-content": "Select"
      })
      .addClass("image")
      .append(
        $('<img></img>')
        .attr({
          width: "100%",
          src: this._videoCatalog.getDataForVideo(0).previewUrl
        })
      ).click(clickCallback);
    $('.left').append(
      previews[0]
    );
    previews[1] = $('<div></div>')
      .attr({
        id: "1",
        "data-content": "Play"
      })
      .addClass("image")
      .append(
        $('<img></img>')
        .attr({
          width: "100%",
          src: this._videoCatalog.getDataForVideo(1).previewUrl
        })
      ).click(clickCallback);
    $('.center').append(
      previews[1]
    );
    previews[2] = $('<div></div>')
      .attr({
        id: "2",
        "data-content": "Select"
      })
      .addClass("image")
      .append(
        $('<img></img>')
        .attr({
          width: "100%",
          src: this._videoCatalog.getDataForVideo(2).previewUrl
        })
      ).click(clickCallback);
    $('.right').append(
      previews[2]
    );
  };

  this.removeVideoPreviews = function () {
    $('.left').empty();
    $('.center').empty();
    $('.right').empty();
  };

};

var sampleApplication;
Bootstrap.registerComponent(
  "SampleApp",
  function (eventBus) {
    var loggedInUser = DefaultUser;
    var videoCatalog = new VideoCatalog();
    var applicationLogger = new Logger("SampleApp", "blue");
    var applicationConfig = Config;
    sampleApplication = new SampleApp(applicationConfig, loggedInUser, videoCatalog, applicationLogger, eventBus);
    sampleApplication.start();
  },
  function () {
    sampleApplication.stop();
    sampleApplication = null;
  },
  1 // low priority
);

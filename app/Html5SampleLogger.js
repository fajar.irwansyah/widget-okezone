/*! (C) 2016 Conviva, Inc. All rights reserved. Confidential and proprietary. */
/*! This is sample code meant to illustrate proper Conviva integration in video applications. */
/*! This file should not be included in video applications as part of integrating Conviva. */

/// On-screen and console logger

var Logger = function (componentName, color) {

	var _componentName = componentName;
	var _color = color;

    this.log = function (message) {
    	var formattedMessage = "[" + _componentName + "] " + message;
    	Logger.logToConsole(formattedMessage);
    	Logger.logToScreen(formattedMessage, _color);
    };

};

Logger.getLogArea = function () {
    return document.getElementById("log");
};

Logger.logToConsole = function (message) {
    console.log(message);
};

Logger.logToScreen = function (message, color) {
    var li = document.createElement('li');
    li.style.color = color;
    li.innerHTML = message;

    var screenLogArea = Logger.getLogArea();
    screenLogArea.appendChild(li);
    screenLogArea.scrollTop = screenLogArea.scrollHeight;
};


/*! (C) 2016 Conviva, Inc. All rights reserved. Confidential and proprietary. */
/*! This is sample code meant to illustrate proper Conviva integration in video applications. */
/*! This file should not be included in video applications as part of integrating Conviva. */

/// Allows consumers to publish and subscribe to application events.
/// Primary publisher is SampleApp.
/// Primary subscriber is ConvivaIntegration.

var EventBus = function () {

  var callbacks = {};

  this.subscribe = function (event, listener) {
    callbacks[event].push(listener);
  };

  this.unsubscribe = function (event, listener) {
    var listenersLeft = [];
    for (var i = 0; i < callbacks[event].length; i++) {
      var existingListener = callbacks[event][i];
      if (existingListener != listener) {
        listenersLeft.push(existingListener);
      }
    }
    callbacks[event] = listenersLeft;
  };

  this.publish = function (event, param1, param2, param3) {
    var callbacksForEvent = callbacks[event];
    for (var i = 0; i < callbacksForEvent.length; i++) {
      var callback = callbacksForEvent[i];
      callback.call(undefined, param1, param2, param3); // synchronous
    }
  };

  function _constr() {
    for (var eventKey in EventBus.Event) {
      var event = EventBus.Event[eventKey];
      callbacks[event] = [];
    }
  }

  _constr.apply(this);

};

EventBus.Event = {
  APPLICATION_START: "APPLICATION_START",
  APPLICATION_STOP: "APPLICATION_STOP",
  VIDEO_PLAYBACK_REQUEST: "VIDEO_PLAYBACK_REQUEST",
  VIDEO_PLAYBACK_CANCELLED: "VIDEO_PLAYBACK_CANCELLED",
  VIDEO_PLAYBACK_FAILED: "VIDEO_PLAYBACK_FAILED",
  VIDEO_PLAYBACK_ENDED: "VIDEO_PLAYBACK_ENDED",
  VIDEO_PLAYER_CREATED: "VIDEO_PLAYER_CREATED",
  VIDEO_PLAYER_DESTROYED: "VIDEO_PLAYER_DESTROYED",

  CONTENT_MUTABLE_METADATA_REQUEST: "CONTENT_MUTABLE_METADATA_REQUEST",
  CONTENT_SET_BITRATE_KBPS_REQUEST: "CONTENT_SET_BITRATE_KBPS_REQUEST",
  CONTENT_SEND_CUSTOM_EVENT_REQUEST: "CONTENT_SEND_CUSTOM_EVENT_REQUEST",
  CONTENT_REPORT_ERROR_REQUEST: "CONTENT_REPORT_ERROR_REQUEST",
  CONTENT_SET_CDN_SERVER_IP: "CONTENT_SET_CDN_SERVER_IP",

  CREATE_AD_SESSION_REQUEST: "CREATE_AD_SESSION_REQUEST",
  AD_MUTABLE_METADATA_REQUEST: "AD_MUTABLE_METADATA_REQUEST",
  AD_SET_BITRATE_KBPS_REQUEST: "AD_SET_BITRATE_KBPS_REQUEST",
  AD_SEND_CUSTOM_EVENT_REQUEST: "AD_SEND_CUSTOM_EVENT_REQUEST",
  AD_REPORT_ERROR_REQUEST: "AD_REPORT_ERROR_REQUEST",
  CLOSE_AD_SESSION_REQUEST: "CLOSE_AD_SESSION_REQUEST"
};

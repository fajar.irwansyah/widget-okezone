/*! (C) 2016 Conviva, Inc. All rights reserved. Confidential and proprietary. */
/*! This is sample code meant to illustrate proper Conviva integration in video applications. */
/*! This file should not be included in video applications as part of integrating Conviva. */

var Bootstrapper = function () {

  var components = [];

  this.registerComponent = function (componentName, componentInitFunction, componentCleanupFunction, componentPriority) {
    components.push({
      name: componentName,
      init: componentInitFunction,
      cleanup: componentCleanupFunction,
      priority: componentPriority
    });
  };

  this.start = function () {
    this._eventBus = new EventBus();

    this.log("--- START ---");
    
    // Higher priority components first
    components.sort(function (c1, c2) { return c2.priority - c1.priority; });

    // Initialize all components
    for (var i = 0; i < components.length; i++) {
      var component = components[i];
      component.init(this._eventBus);
    }
  };

  this.stop = function () {
    this.log("--- STOP ---");

    // Lower priority components first
    components.sort(function (c1, c2) { return c1.priority - c2.priority; });

    // Cleanup all components
    for (var i = 0; i < components.length; i++) {
      var component = components[i];
      component.cleanup();
    }

    this._eventBus = null;
  };

  this.log = function (message) {
    Logger.logToConsole(message);
    Logger.logToScreen(message, "red");
  };

};

var Bootstrap = new Bootstrapper();

$(document).ready(function() {
  var started = false;
  $('#start-stop').click(function () {
    if (started) {
      Bootstrap.stop();
      started = false;
      $('#start-stop').html("Start Application");
    } else {
      Bootstrap.start();
      started = true;
      $('#start-stop').html("Stop Application");
    }
  });
});


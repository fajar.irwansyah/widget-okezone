/*! (C) 2016 Conviva, Inc. All rights reserved. Confidential and proprietary. */
/*! This is sample code meant to illustrate proper Conviva integration in video applications. */
/*! This file should not be included in video applications as part of integrating Conviva. */

/// Default user for this application

var DefaultUser = {
    id: "defaultUser",
    accountType: "free",
    subscriptionType: "monthly"
};


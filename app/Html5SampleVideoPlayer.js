/*! (C) 2016 Conviva, Inc. All rights reserved. Confidential and proprietary. */
/*! This is sample code meant to illustrate proper Conviva integration in video applications. */
/*! This file should not be included in video applications as part of integrating Conviva. */

/// Handles the HTML5 video element and its playback flow

var VideoPlayer = function (jqueryContainer, videoStreamUrl, endedCallback, failureCallback) {

  var container = jqueryContainer;
  var streamUrl = videoStreamUrl;
  var endCallback = endedCallback;
  var failCallback = failureCallback;
  var jqueryVideoElement = null;
  var videoElement = null;

  this.create = function () {
    jqueryVideoElement = $('<video></video>')
      .attr({
        src: streamUrl,
        preload: "none",
        // autoplay: "true", // start playing immediately
        width: "100%",
        controls: "true",
        muted: "true", // for my sanity
      })
      .hide(); // hide until ready by default
    container.append(jqueryVideoElement);

    this.addVideoListeners();

    videoElement = jqueryVideoElement.get(0);
  };

  this.getVideoElement = function () {
    return videoElement;
  };

  this.play = function () {
    videoElement.play();
  };

  this.stop = function () {
    videoElement.pause();
    this.removeVideoListeners();
  };

  this.addVideoListeners = function () {
    jqueryVideoElement
    .on( "ended", function () {
      setTimeout(endCallback, 1); // notify SampleApp that the video is over.
    })
    .on( "error", function () {
      setTimeout(failCallback, 1); // HTML5 errors are always fatal. Notify SampleApp that video playback failed.
    });
  };

  this.removeVideoListeners = function () {
    jqueryVideoElement
    .off( "ended" )
    .off( "error" );
  };

  this.remove = function () {
    this.stop();
    jqueryVideoElement.remove();
  };

  this.show = function () {
    jqueryVideoElement.show();
  };

};


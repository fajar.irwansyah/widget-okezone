var systemSettings = null;
var systemInterface = null;
var systemFactory = null;
var clientSettings = null;
var client = null;

var contentSessionKey = Conviva.Client.NO_SESSION_KEY;
var adSessionKey = Conviva.Client.NO_SESSION_KEY;

var html5PlayerInterface = null;
var adPlayerInterface = null;

var playerStateManager = null;
var adSessionPlayerStateManager = null;

var contentConfigIndex = 0;
var contentBitrateIndex = 0;
var contentSessionEventsIndex = 0;
var contentReportErrorIndex = 0;

var adConfigIndex = 0;
var adBitrateIndex = 0;
var adSessionEventsIndex = 0;
var adReportErrorIndex = 0;

var prerollPlayed = false;
var postrollPlayed = false;

var videoElement = document.getElementById('videoPlayer');

// Init Conviva Client and Create Content Session
initConvivaClient();
createContentSession();

videoElement.addEventListener('error', function() {
    // Not recommended to cleanupSession on error event as the content might recover from error mode
    //cleanupSession();
});
videoElement.addEventListener('play', function() {
    // DE-2594
    // Play event is triggered on "replay" of content. It is suggested to create a session if there is none created earlier
    if(client == null || contentSessionKey == Conviva.Client.NO_SESSION_KEY) {
        initConvivaClient();
        createContentSession();
    }
});

videoElement.addEventListener('ended', function() {
    // Handling Postroll Scenario
    if (configs.content.postrollEnabled && !postrollPlayed) {
        detachPlayer(contentSessionKey, playerStateManager);
        postrollPlayed = true;
        createAdSession(true);
        setTimeout(function() {
            // CleanUp Ad Session followed by CleanUp of Content Session
            cleanupAdSession();
            cleanupContentSession();
        }, 5000);
    } else { // Cleanup Content Session if postroll is not enabled
        cleanupContentSession();
    }
});

function initConvivaClient() {
    systemSettings = new Conviva.SystemSettings();
    //systemSettings.logLevel = Conviva.SystemSettings.LogLevel.DEBUG;
    systemInterface = new Conviva.SystemInterface(
        new Conviva.Impl.Html5Time(),
        new Conviva.Impl.Html5Timer(),
        new Conviva.Impl.Html5Http(),
        new Conviva.Impl.Html5Storage(),
        new Conviva.Impl.Html5Metadata(),
        new Conviva.Impl.Html5Logging()
    );

    systemFactory = new Conviva.SystemFactory(systemInterface, systemSettings);

    //Customer integration
    clientSettings = new Conviva.ClientSettings(configs.content.CUSTOMER_KEY);
    if (configs.content.gatewayUrl != undefined) {
        clientSettings.gatewayUrl = configs.content.gatewayUrl;
    }

    client = new Conviva.Client(clientSettings, systemFactory);
}

// Create a Conviva monitoring session.
function createContentSession() {
    contentSessionKey = client.createSession(buildConvivaContentMetadata());

    playerStateManager = client.getPlayerStateManager();
    html5PlayerInterface = new Conviva.Impl.Html5PlayerInterface(playerStateManager, videoElement, systemFactory);

    // Handling Preroll Scenario
    if (configs.content.prerollEnabled) {
        prerollPlayed = true;
        client.adStart(contentSessionKey,
                       Conviva.Client.AdStream.SEPARATE,
                       Conviva.Client.AdPlayer.CONTENT,
                       Conviva.Client.AdPosition.PREROLL);
        createAdSession(false);
    } else {
        attachPlayer(contentSessionKey, playerStateManager);
    }
}

function attachPlayer(sessionKey, stateManager) {
    if (client != null && sessionKey != Conviva.Client.NO_SESSION_KEY) {
        client.attachPlayer(sessionKey, stateManager);
    }
}

function detachPlayer(sessionKey) {
    if (client != null && sessionKey != Conviva.Client.NO_SESSION_KEY) {
        client.detachPlayer(sessionKey);
    }
}

function buildConvivaContentMetadata() {
    var contentMetadata = new Conviva.ContentMetadata();

    var credentials = configs["content"];
    if (credentials != null) {
        //Create metadata
        buildContentMetadata(credentials, contentMetadata);
    }
    return contentMetadata;
}

function contentUpdateContentMetadata() {
    if (client != null) {
        client.updateContentMetadata(contentSessionKey, buildMutableMetadata());
    }
}

function contentSetBitrateKbps() {
    var credentials = configs["contentMidStreamBitrateKbps"];
    if (credentials != null) {
        if (credentials[contentBitrateIndex].bitrateKbps > 0) {
            playerStateManager.setBitrateKbps(credentials[contentBitrateIndex].bitrateKbps);
        }
        contentBitrateIndex = contentBitrateIndex + 1;
        if(contentBitrateIndex >= credentials.length ) {
            contentBitrateIndex = 0;
        }
    }
}

function contentSendCustomEvent() {
    var credentials = configs["contentSessionEvents"];
    if (credentials != null) {
        client.sendCustomEvent(contentSessionKey,
            credentials[contentSessionEventsIndex].name,
            credentials[contentSessionEventsIndex].attr);
        contentSessionEventsIndex = contentSessionEventsIndex + 1;
        if(contentSessionEventsIndex >= credentials.length) {
            contentSessionEventsIndex = 0;
        }
    }
}

function contentReportError() {
    var credentials = configs["contentReportError"];
    if (credentials != null) {
        client.reportError(contentSessionKey,
            credentials[contentReportErrorIndex].errorMessage,
            credentials[contentReportErrorIndex].errorSeverity);
        contentReportErrorIndex = contentReportErrorIndex + 1;
        if(contentReportErrorIndex >= credentials.length) {
            contentReportErrorIndex = 0;
        }
    }
}

function cleanupContentSession() {
    // Added to ensure cleanupAdSession taken care during cleanupContentSession
    postrollPlayed = true;
    cleanupAdSession();

    if (contentSessionKey != Conviva.Client.NO_SESSION_KEY) {
        html5PlayerInterface.cleanup();
        html5PlayerInterface = null;

        client.releasePlayerStateManager(playerStateManager);
        playerStateManager = null;

        client.cleanupSession(contentSessionKey);
        contentSessionKey = Conviva.Client.NO_SESSION_KEY;
    }
    // Release Conviva Client if required during cleanupSession or only during exiting of application
    releaseConvivaClient();
}

function releaseConvivaClient() {
    if (client != null) {
        client.release();
        client = null;
    }

    if (systemFactory != null) {
        // If Client was the only consumer of systemFactory, release systemFactory as well.
        systemFactory.release();
        systemFactory = null;
    }
}

function createAdSession(detach) {
    // detachPlayer:true need to invoked only for Midroll/Postroll Scenarios
    if (detach) {
        detachPlayer(contentSessionKey);
    }

    // Ad Session need to be created followed by creating playerStateManager and
    // Interface object followed by attachPlayer
    if (adSessionKey == Conviva.Client.NO_SESSION_KEY) {
        adSessionKey = client.createAdSession(contentSessionKey, buildConvivaAdMetadata());

        adSessionPlayerStateManager = client.getPlayerStateManager();
        adPlayerInterface = new Conviva.Impl.Html5PlayerInterface(adSessionPlayerStateManager, videoElement, systemFactory);

        attachPlayer(adSessionKey, adSessionPlayerStateManager);
    }
}

function cleanupAdSession() {
    // Releasing playerStateManager, interface cleanup need to be invoked before cleanupSession()
    if (adSessionKey != Conviva.Client.NO_SESSION_KEY) {
        adPlayerInterface.cleanup();
        adPlayerInterface = null;

        client.releasePlayerStateManager(adSessionPlayerStateManager);
        adSessionPlayerStateManager = null;

        client.cleanupSession(adSessionKey);
        adSessionKey = Conviva.Client.NO_SESSION_KEY;
    }

    // adEnd need to invoked while resuming content for Preroll scenario
    if (configs.content.prerollEnabled && prerollPlayed) {
        client.adEnd(contentSessionKey);
        prerollPlayed = false;
    }

    // attachPlayer need to be invoked while resuming content for Preroll/Midroll scenarios
    if(!postrollPlayed) {
        attachPlayer(contentSessionKey, playerStateManager);
    }
}

function adUpdateContentMetadata() {
    if (client != null) {
        client.updateContentMetadata(adSessionKey, buildConvivaMidStreamAdMetadata());
    }
}

function adSetBitrateKbps() {
    var credentials = configs["adMidStreamBitrateKbps"];
    if (credentials != null) {
        if (credentials[adBitrateIndex].adMediaFileBitrate > 0) {
            adSessionPlayerStateManager.setBitrateKbps(credentials[adBitrateIndex].adMediaFileBitrate);
        }
        adBitrateIndex = adBitrateIndex + 1;
        if(adBitrateIndex >= credentials.length) {
            adBitrateIndex = 0;
        }
    }
}

function adSendCustomEvent() {
    var credentials = configs["adSessionEvents"];
    if (credentials != null) {
        client.sendCustomEvent(adSessionKey,
            credentials[adSessionEventsIndex].name,
            credentials[adSessionEventsIndex].attr);
        adSessionEventsIndex = adSessionEventsIndex + 1;
        if(adSessionEventsIndex >= credentials.length) {
            adSessionEventsIndex = 0;
        }
    }
}

function adReportError() {
    var credentials = configs["adReportError"];
    if (credentials != null) {
        client.reportError(adSessionKey,
            credentials[adReportErrorIndex].errorMessage,
            credentials[adReportErrorIndex].errorSeverity);
        adReportErrorIndex = adReportErrorIndex + 1;
        if(adReportErrorIndex >= credentials.length) {
            adReportErrorIndex = 0;
        }
    }
}

function buildConvivaAdMetadata() {
  var adMetadata = new Conviva.ContentMetadata();
  //For testing purposes, pick the metadata from the JSON HTML5ConvivaCredentials.
  var credentials = configs["ad"];
  if(credentials) {
      buildAdMetadata(credentials, adMetadata);
  }
  return adMetadata;
}

function buildContentMetadata(credentials, contentMetadata) {
    if (credentials.assetName != null) {
        contentMetadata.assetName = credentials.assetName;
    }
    if (credentials.url != null) {
        contentMetadata.streamUrl = credentials.url;
    }
    if (credentials.live != null) {
        contentMetadata.streamType = credentials.live ? Conviva.ContentMetadata.StreamType.LIVE : Conviva.ContentMetadata.StreamType.VOD;
    }
    if (credentials.bitrateKbps != null) {
        contentMetadata.defaultBitrateKbps = credentials.bitrateKbps; // in Kbps
    }
    if (credentials.applicationName != null) {
        contentMetadata.applicationName = credentials.applicationName;
    }
    if (credentials.viewerId != null) {
        contentMetadata.viewerId = credentials.viewerId;
    }
    if(credentials.tags != undefined) {
        contentMetadata.custom = credentials.tags;
    }
    if(credentials.contentLength != undefined) {
        contentMetadata.duration = credentials.contentLength;
    }
    if(credentials.encodedFps != undefined) {
        contentMetadata.encodedFrameRate = credentials.encodedFps;
    }
    if(credentials.defaultResource != undefined) {
        contentMetadata.defaultResource = credentials.defaultResource;
    }
}

function buildAdMetadata(credentials, adMetadata) {
    if (credentials.adTitle != null) {
        adMetadata.assetName = credentials.adTitle;
    }
    if (credentials.adCustom != null && Object.keys(credentials.adCustom).length > 0) {
        for (var tagKey in credentials.adCustom) {
            adMetadata.custom[tagKey] = credentials.adCustom[tagKey];
        }
    }
    if (credentials.adMediaFileBitrate != null) {
        adMetadata.defaultBitrateKbps = credentials.adMediaFileBitrate;
    }
    if (credentials.adMediaFile != null) {
        adMetadata.streamUrl = credentials.adMediaFile;
    }
    if (credentials.adDuration != null) {
        adMetadata.duration = credentials.adDuration;
    }
    if (credentials.adEncodedFrameRate != null) {
        adMetadata.encodedFrameRate = credentials.adEncodedFrameRate;
    }
    if (credentials.adIsLive != null) {
        adMetadata.streamType = credentials.adIsLive ? Conviva.ContentMetadata.StreamType.LIVE : Conviva.ContentMetadata.StreamType.VOD;
    }
    if(credentials.adDefaultResource != undefined) {
        adMetadata.defaultResource = credentials.adDefaultResource;
    }
    if (credentials.adApplicationName != null) {
        adMetadata.applicationName = credentials.adApplicationName;
    }
    if (credentials.adViewerId != null) {
        adMetadata.viewerId = credentials.adViewerId;
    }
}

function buildConvivaMidStreamAdMetadata() {
  var adMetadata = new Conviva.ContentMetadata();
  //For testing purposes, pick the metadata from the JSON HTML5ConvivaCredentials.
  var credentials = configs["midStreamAdMetadata"];

  if(credentials) {
      buildAdMetadata(credentials[adConfigIndex], adMetadata);

      adConfigIndex = adConfigIndex + 1;
      if(adConfigIndex >= credentials.length) {
          adConfigIndex = 0;
      }
  }
  return adMetadata;
}

function buildMutableMetadata() {
    var contentMetadata = new Conviva.ContentMetadata();

    //For testing purposes, pick the metadata from the JSON HTML5ConvivaCredentials.
    var credentials = configs["midStreamMetadata"];
    if(credentials) {
        buildContentMetadata(credentials[contentConfigIndex], contentMetadata);

        contentConfigIndex = contentConfigIndex + 1;
        if(contentConfigIndex >= credentials.length) {
            contentConfigIndex = 0;
        }
    }
    return contentMetadata;
}

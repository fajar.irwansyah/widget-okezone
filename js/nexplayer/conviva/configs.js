var configs = {
    content: {
        bitrateKbps: 200,
        assetName: "HTML5 Native Sample application",
        live: true,
        url: "http://content.bitsontherun.com/videos/QieeAOba-jt16QglC.mp4",
        contentLength: 10,
        encodedFps: 20,
        applicationName:"HTML5 Native Sample",
        viewerId:"19227481",
        defaultResource: "ANEVIA",
        tags: {"tag1":"val", "tagBool":"true", "tagInt":"50"},

        CUSTOMER_KEY: "25f1bbffdca1e0a4d059c0d8f802234bbb97dcf1",
        gatewayUrl:"https://mnc-test.testonly.conviva.com",
        prerollEnabled: false, // Set to true for simulating preroll scenario
        postrollEnabled: false // Set to true for simulating postroll scenario
    },
    midStreamMetadata:[{
        bitrateKbps: 300,
        assetName: "Bikers_1500",
        live: false,
        url: "http://sample/vod/Bikers_1500.mp4",
        contentLength: 20,
        encodedFps: 24,
        applicationName:"App Name 1",
        viewerId:"ConvivaQE",
        defaultResource: "OTHER",
        tags: {"tag1":"val1", "tagBool":"false", "tagInt":"100"}
    } , {
        bitrateKbps: 400,
        assetName: "SintelLong",
        live: true,
        url: "http://sample/vod/sintel-long-900.m3u8",
        contentLength: 100,
        encodedFps: 44,
        applicationName:"App Name 2",
        viewerId:"ConvivaQE2",
        defaultResource: "Conviva",
        tags: {"tag2":"val2", "tag1":"val1a", "tagBool":"true", "tagInt":"200" }
    } , {
        bitrateKbps: 500,
        assetName: "Sintel",
        live: false,
        url: "http://sample/vod/sintel.ism",
        contentLength: 200,
        encodedFps: 50,
        applicationName:"App Name 3",
        viewerId:"ConvivaQE3",
        defaultResource: "OTHER",
        tags: {"tag2": "val2b", "tag3": "val3"}
    }],
    ad: {
        adTitle: "Test Ad Title",
        adMediaFileBitrate: 200,
        adMediaFile: "http://sample.m3u8",
        adDuration: 60,
        adEncodedFrameRate: 20,
        adApplicationName: "AdApplicationName",
        adViewerId: "AdViewerId",
        adIsLive: false,
        adDefaultResource: "AdResource",
        adCustom: {
            newtag1:"value1",
            newtag2: "false",
            newtag3: "100",
            "c3.ad.breakId": "f7000b6e8e904afc8c6e740edbabe411",
            "c3.ad.technology": Conviva.Client.AdTechnology.CLIENT_SIDE,
            "c3.ad.id": "12345678",
            "c3.ad.unitName": "UnitName",
            "c3.ad.position": Conviva.Client.AdPosition.MIDROLL,
            "c3.ad.type": Conviva.Client.AdType.BLACKOUT_SLATE,
            "c3.ad.servingType": Conviva.Client.AdServingType.INLINE,
            "c3.ad.sequence": "3",
            "c3.ad.category": "National",
            "c3.ad.classification": "Classification",
            "c3.ad.system": "Freewheel",
            "c3.ad.advertiser": "Advertiser",
            "c3.ad.advertiserName": "AdvertiserName",
            "c3.ad.advertiserCategory": "AdvertiserCategory",
            "c3.ad.advertiserId": "AdvertiserId",
            "c3.ad.creativeId": "CreativeId",
            "c3.ad.creativeName": "CreativeName",
            "c3.ad.mediaFileApiFramework": "FrameworkName",
            "c3.ad.campaignName": "Campaign",
            "c3.ad.dayPart": "Breakfast"
        }
    },
    midStreamAdMetadata:[{
      adTitle: "SampleUrl2",
        adMediaFile: "http://sample_url2.m3u8",
        adDuration: 120,
        adEncodedFrameRate: 40,
        adApplicationName:"AdApplicationName2",
        adViewerId:"ConvivaQE2",
        adIsLive: true,
        adDefaultResource: "AdResource2",
        adCustom: {
            newtag4:"value4",
            newtag1: "true",
            newtag6: "200",
            "c3.ad.breakId": "f7000b6e8e904afc8c6e740edbabe411_2",
            "c3.ad.technology": Conviva.Client.AdTechnology.SERVER_SIDE,
            "c3.ad.id": "87654321",
            "c3.ad.unitName": "UnitName2",
            "c3.ad.position": Conviva.Client.AdPosition.PREROLL,
            "c3.ad.type": Conviva.Client.AdType.TECHNICAL_DIFFICULTIES_SLATE,
            "c3.ad.servingType": Conviva.Client.AdServingType.WRAPPER,
            "c3.ad.sequence": "6",
            "c3.ad.category": "Regional",
            "c3.ad.classification": "Classification2",
            "c3.ad.system": "GoogleIMA3",
            "c3.ad.advertiser": "Advertiser2",
            "c3.ad.advertiserName": "AdvertiserName2",
            "c3.ad.advertiserCategory": "AdvertiserCategory2",
            "c3.ad.advertiserId": "AdvertiserId2",
            "c3.ad.creativeId": "CreativeId2",
            "c3.ad.creativeName": "CreativeName2",
            "c3.ad.mediaFileApiFramework": "FrameworkName2",
            "c3.ad.campaignName": "Campaign2",
            "c3.ad.dayPart": "Daytime Late Morning"
        },
    } , {
      adTitle: "SampleUrl3",
        adMediaFile: "http://sample_url3.m3u8",
        adDuration: 180,
        adEncodedFrameRate: 60,
        adApplicationName:"AdApplicationName3",
        adViewerId:"ConvivaQE3",
        adIsLive: false,
        adDefaultResource: "AdResource3",
        adCustom: {
            newtag4:"value7",
            newtag8: "false",
            newtag6: "400",
            "c3.ad.breakId": "f7000b6e8e904afc8c6e740edbabe411_3",
            "c3.ad.technology": Conviva.Client.AdTechnology.CLIENT_SIDE,
            "c3.ad.id": "000000",
            "c3.ad.unitName": "UnitName3",
            "c3.ad.position": Conviva.Client.AdPosition.POSTROLL,
            "c3.ad.type": Conviva.Client.AdType.COMMERCIAL_SLATE,
            "c3.ad.servingType": Conviva.Client.AdServingType.INLINE,
            "c3.ad.sequence": "9",
            "c3.ad.category": "International",
            "c3.ad.classification": "Classification3",
            "c3.ad.system": "mpxConsole",
            "c3.ad.advertiser": "Advertiser3",
            "c3.ad.advertiserName": "AdvertiserName3",
            "c3.ad.advertiserCategory": "AdvertiserCategory3",
            "c3.ad.advertiserId": "AdvertiserId3",
            "c3.ad.creativeId": "CreativeId3",
            "c3.ad.creativeName": "CreativeName3",
            "c3.ad.mediaFileApiFramework": "FrameworkName3",
            "c3.ad.campaignName": "Campaign3",
            "c3.ad.dayPart": "National Prime Time Late Fringe"
        }
    }],
    adMidStreamBitrateKbps:[{
        adMediaFileBitrate: 400,
    }, {
        adMediaFileBitrate: 600,
    }],
    contentMidStreamBitrateKbps:[{
        bitrateKbps: 400,
    }, {
        bitrateKbps: 600,
    }],
    contentSessionEvents:[{
        "name": Conviva.Events.AD_REQUESTED,
        "attr": {
            "contentAssetId": "simp_season2_23dyfusv"
        }
    } , {
        "name": Conviva.Events.AD_RESPONSE,
        "attr": {
            "success": "true",
            "adId": "1674387534",
            "adName": "PrerollAd1"
        }
    } , {
        "name": Conviva.Events.AD_SLOT_STARTED,
        "attr": {
            "customId": "Preroll_1"
        }
    } , {
        "name": Conviva.Events.AD_SLOT_ENDED,
        "attr": {
            "customId": "Preroll_1"
        }
    } , {
        "name": Conviva.Events.CONTENT_PAUSED,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.CONTENT_RESUMED,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.POD_START,
        "attr": {
            "podDuration": "60",
            "podPosition": "Pre-roll",
            "podIndex": "1",
            "absoluteIndex": "1"
        }
    } , {
        "name": Conviva.Events.POD_END,
        "attr": {
            "podDuration": "60",
            "podPosition": "Pre-roll",
            "podIndex": "1",
            "absoluteIndex": "1"
        }
    }],
    adSessionEvents:[{
        "name": Conviva.Events.AD_ATTEMPTED,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.AD_IMPRESSION_START,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.AD_IMPRESSION_END,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.AD_FIRST_QUARTILE,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.AD_MID_QUARTILE,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.AD_THIRD_QUARTILE,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.AD_COMPLETE,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.AD_END,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.AD_SKIPPED,
        "attr": {
            "adId": "6491132"
        }
    } , {
        "name": Conviva.Events.AD_ERROR,
        "attr": {
            "adId": "6491132",
            "error": Conviva.ErrorType.ERROR_TIMEOUT
        }
    } , {
        "name": Conviva.Events.AD_ERROR,
        "attr": {
            "adId": "6491132",
            "error": Conviva.ErrorType.ERROR_UNKNOWN
        }
    }],
    contentReportError:[{
        "errorMessage": Conviva.ErrorType.ERROR_UNKNOWN,
        "errorSeverity": Conviva.Client.ErrorSeverity.FATAL
    } , {
        "errorMessage": Conviva.ErrorType.ERROR_TIMEOUT,
        "errorSeverity": Conviva.Client.ErrorSeverity.WARNING
    }],
    adReportError:[{
        "errorMessage": Conviva.ErrorType.ERROR_PARSE,
        "errorSeverity": Conviva.Client.ErrorSeverity.FATAL
    } , {
        "errorMessage": Conviva.ErrorType.ERROR_IO,
        "errorSeverity": Conviva.Client.ErrorSeverity.WARNING
    }]
};

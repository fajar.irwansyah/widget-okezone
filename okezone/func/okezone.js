
// var configs = require('../conf/config-prd.js');
var urlbase = "https://api.mncnow.id"

var getToken = function() {
    params = {
        "device_id": "okezone",
        "password": "n3B6XXGuh8uCoqHj2QJH",
        "platform": "android",
        "username": "okezone.development@gmail.com"
    }

    var ItemJSON;
    var res;

    ItemJSON = JSON.stringify(params)
    URL = urlbase + "/api/v/login"

    var xmlhttp = new XMLHttpRequest();
    // xmlhttp.onreadystatechange = callbackFunction(xmlhttp);
    xmlhttp.open("POST", URL, false);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(ItemJSON);
    res =  JSON.parse(xmlhttp.responseText);
    // this.token = res.data.access_token;
    // this.user_id = res.data.user.id;
    return xmlhttp.responseText
}

var getContent = function(token, id) {

    URL = configs.content.urlbase + "/api/v5/contents/" + id 
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = callbackFunction(xmlhttp);
    xmlhttp.open("GET", URL, false);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.setRequestHeader("Authorization", token);
    xmlhttp.send();
    resJSON =  xmlhttp.responseText;
    dataObj = JSON.parse(resJSON);
    // user_id = res.data.user.id;
    // document.getElementById("response").innerHTML = resJSON;
    this.id = dataObj.data.id;
    this.content_core_id = dataObj.data.content_core_id;
    this.content_core_id = dataObj.data.content_core_id;
    this.title = dataObj.data.title;
    this.title = dataObj.data.type;
    this.title = dataObj.data.rating;
    this.poster = dataObj.data.banner_url;
    this.banner = dataObj.data.image_url;
}

var getPlayer = function(token, id) {
    URL =  configs.content.urlbase + "/api/v4/player/" + id
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = callbackFunction(xmlhttp);
    xmlhttp.open("GET", URL, false);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.setRequestHeader("Authorization", token);
    xmlhttp.send();
    resJSON =  xmlhttp.responseText;
    dataObj = JSON.parse(resJSON);
    this.contentCore_id = dataObj.data.player.contentCore_id;
    this.drm = dataObj.data.player.drm;
    this.a = dataObj.data.player.a;
    this.k = dataObj.data.player.k;
    this.mpd = dataObj.data.player.mpd;
    this.hls = dataObj.data.player.hls;
    this.play_url = dataObj.data.player.play_url;
}

var getHomepage = function(token) {
    URL =  configs.content.urlbase + "/api/v5/homepage?showAll=true"
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = callbackFunction(xmlhttp);
    xmlhttp.open("GET", URL, false);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.setRequestHeader("Authorization", token);
    xmlhttp.send();
    resJSON =  xmlhttp.responseText;
    dataObj = JSON.parse(resJSON);
    return dataObj;
    // this.id = dataObj.data.id;
    // this.description = dataObj.data.description;
    // this.title = dataObj.data.title;
    // this.title_en = dataObj.data.title_en;
    // this.type = dataObj.data.type;
}

var getHomepageDetail = function(token, id) {
    URL =  configs.content.urlbase + "/api/v5/homepage" + id + "/contents"
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = callbackFunction(xmlhttp);
    xmlhttp.open("GET", URL, false);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.setRequestHeader("Authorization", token);
    xmlhttp.send();
    resJSON =  xmlhttp.responseText;
    dataObj = JSON.parse(resJSON);
    return dataObj;
    // this.id = dataObj.data.id;
    // this.content_core_id = dataObj.data.content_core_id;
    // this.title = dataObj.data.title;
    // this.type = dataObj.data.type;
    // this.rating = dataObj.data.rating;
    // this.category = dataObj.data.category;
    // this.content_core_id = dataObj.data.content_core_id;
    // this.synopsis = dataObj.data.synopsis;
    // this.year = dataObj.data.year;
    // this.poster = dataObj.data.image_url;
    // this.banner = dataObj.data.url_wide_poster;
}